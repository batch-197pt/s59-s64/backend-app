const Order = require('../models/Order');
const auth = require('../auth')
const User = require('../models/User')
const Product = require('../models/Product');


//Create Order
module.exports.createOrder = async (userData, reqBody) => {
		
	if(userData.isAdmin){
		return false
	} else {
		function getAmount(value1, value2) {
			if(value2 === undefined){
				value2 = 1;
			}
			let myAmount = value1 * value2
			return myAmount
		}


		let result = await Product.findById(reqBody.productId).populate("price", {path: "price"});
		let newOrder = new Order({
			userId: userData.id,
			products: [{
				productId: reqBody.productId,
				quantity: reqBody.quantity
			}],
			totalAmount: getAmount(result.price, reqBody.quantity)
		})

		return newOrder.save().then((data, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	}
};



//Retrieve all orders
module.exports.getAllOrders = (userData) => {
	if(userData.isAdmin) {
		return Order.find({}).then(result => {
			return result
		})
	} else {
		return false
	}
};


//Retrieve User Order
module.exports.retrieveUserOrder = async (userData) => {
	if(userData.isAdmin == false) {
		let user = await User.findById(userData.id);
		let myOrder = await Order.find({userId: user});
		return myOrder;
	} else {
		return false
	}
};


//Remove User Order
module.exports.removeUserOrder = (idParams, userData) => {
	if(userData.isAdmin){
		return false
	}else{
		return Order.findByIdAndDelete(idParams.orderId).then((deletedOrder, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	}
}


//Update Order Information
module.exports.updateOrder = async (idParams, reqBody, userData) => {
	console.log(idParams)
	console.log(reqBody)
	console.log(userData.isAdmin)
	if(userData.isAdmin) {
		return false
	} else {

		function getTotalAmount(value1, value2) {
			if(value2 === undefined){
				value2 = 1;
			}
			let myAmount = value1 * value2
			return myAmount
		}

		let result = await Product.findById(reqBody.productId).populate("price", {path: "price"});
		let updatedOrder = {
			id: idParams,
			userId: userData.id,
			products: [{
				productId: reqBody.productId,
				quantity: reqBody.quantity
			}],
			totalAmount: getTotalAmount(result.price, reqBody.quantity)
		}

		return Order.findByIdAndUpdate(idParams.orderId, updatedOrder).then((data, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	}
};