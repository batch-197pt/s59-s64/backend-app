const Product = require('../models/Product');
const auth = require('../auth');
const User = require('../models/User');
const Order = require('../models/Order');

//Add Product
module.exports.addProduct = (reqBody, userData) => {

	return User.findById(userData.id).then(result => {
		if(userData.isAdmin == false) {
			return false
		} else {
			let newProduct = new Product({
				name: reqBody.name,
				model: reqBody.model,
				description: reqBody.description,
				price: reqBody.price
			})

			return newProduct.save().then((data, error) =>{
				if (error) {
					return false
				} else {
					return true
				}
			})
		}
	})
};


//Get ALL Products
module.exports.getAllProducts = (data) => {
	console.log(data)
	if (data.isAdmin) {
		return Product.find({}).then(result => {
			return result
		}) 
	} else {
		return false
	}
};


//Get All ACTIVE Products
module.exports.getAllActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
};



//Retrieve Single Product
module.exports.getSingleProduct = (idParams) => {
	return Product.findById(idParams.productId).then(result => {
		return result;
	})
};


//Update Product Information
module.exports.updateProduct = (idParams, reqBody, userData) => {
	if(userData.isAdmin == false) {
		return false
	} else {
		let updatedProduct = {
			name: reqBody.name,
			model: reqBody.model,
			description: reqBody.description,
			price: reqBody.price,
			isActive: reqBody.isActive
		}

		return Product.findByIdAndUpdate(idParams.productId, updatedProduct).then((updatedProduct, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	}
};


//Archive Product
module.exports.archiveProduct = (idParams, reqBody, userData) => {
	if(userData.isAdmin){

		let archivedProduct = {
			isActive: reqBody.isActive
		}

		return Product.findByIdAndUpdate(idParams.productId, archivedProduct).then((archivedProduct, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return false
	}
};


