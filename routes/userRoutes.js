const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

//User Registration Route

router.post("/registration", (req, res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

//User Login Route
router.post("/login", (req, res) => {
	userController.userLogin(req.body).then(resultFromController => res.send(resultFromController))
});


//Retrieve ALL users (ADMIN)
router.get('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.retrieveAllUser(userData).then(resultFromController => res.send(resultFromController))
});


//Retrieve user details
router.get('/:userId', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.retrieveUser(userData, req.params).then(resultFromController => res.send(resultFromController))
});


//set user as admin
router.put('/:userId', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.setAsAdmin(userData, req.params, req.body).then(resultFromController => res.send(resultFromController))
});




module.exports = router;