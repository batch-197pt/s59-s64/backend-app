const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth');

//create order
router.post('/add', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	orderController.createOrder(userData, req.body).then(resultFromController => res.send(resultFromController))
});



//Retrieve all orders (ADMIN)
router.get('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	orderController.getAllOrders(userData).then(resultFromController => res.send(resultFromController))
});


//Retrieve Authenticated User's Order
router.get('/user', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	orderController.retrieveUserOrder(userData).then(resultFromController => res.send(resultFromController))
});



//Remove User Order
router.delete('/:orderId/remove', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.removeUserOrder(req.params, userData).then(resultFromController => res.send(resultFromController))
});


//Update Order
router.put("/:orderId/update", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.updateOrder(req.params, req.body, userData).then(resultFromController => res.send(resultFromController))
});

module.exports = router;