//EXPRESS Setup
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

//For Routes access
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');


const app = express();
const port = process.env.PORT || 8000;


//MIDDLEWARES
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


//URI
app.use('/users', userRoutes)
app.use('/products', productRoutes)
app.use('/orders', orderRoutes)

//Mongoose Connection
mongoose.connect(`mongodb+srv://gperey:admin123@zuitt-batch197.jvr3p9k.mongodb.net/capstone2?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

const db = mongoose.connection

db.on('error', () => console.log('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));


app.listen(port, () => console.log(`Server connected at port ${port}`));